﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    //UI
    public Slider forceSlider;
    public Slider effectSlider;
    public Text angularText;

    public Transform target;
    private Vector3 initTargetPos;
    public int maxDistanceBall = 50;

    private Vector3 shootDir;
    private Vector3 force;
    private Vector3 initVel, endVel;
    private Vector3 angularVel, rotAxis;
    private Vector3 kickVector;

    private Vector3 gravity;
    private bool cogiendoFuerza = false;
    private bool shooting = false;

    private Vector3 initPos;
    public float mass = 20f;
    public float radius = 1;
    private float timeForceAplied = 0.1f;
    private float inertiaMoment;

    private float km = 0.5f * 1.23f * 0.25f * 3.14f;
    private Vector3 magnusForce;
    private Vector3 totalForce;

    public Transform ballTarget;
    private bool needStop = true;

    public Object greyArrow;
    private GameObject gArrw;
    public Object greenArrow;
    private GameObject grnArrw;
    public Object redArrow;
    private GameObject rArrw;
    public Object bluePoints;
    private GameObject bPnts;
    private bool drawArrows = false;

    // Start is called before the first frame update
    void Start() {

        initPos = transform.position;
        initTargetPos = target.position;
        gravity = new Vector3(0, -9.8f * mass, 0);
        forceSlider.maxValue = mass * 400;
        forceSlider.minValue = mass * 200;

        inertiaMoment = (0.667f) * mass * (radius * radius);

    }

    // Update is called once per frame
    void Update() {

        if (Input.GetKeyDown(KeyCode.I)) drawArrows = !drawArrows;

        if (Input.GetKey(KeyCode.Space)) {
            if (!cogiendoFuerza && forceSlider.value < forceSlider.maxValue) {
                forceSlider.value++;
                if (forceSlider.value == forceSlider.maxValue) cogiendoFuerza = true;
            } else if (cogiendoFuerza && forceSlider.value > forceSlider.minValue) {
                forceSlider.value--;
                if (forceSlider.value == forceSlider.minValue) cogiendoFuerza = false;
            }
        } else if (Input.GetKeyUp(KeyCode.Space)) {
            shooting = true;
            shootDir = (target.position - transform.position).normalized;
            force = shootDir * (forceSlider.value * 0.1f);
            initVel = (force * timeForceAplied) / mass;

            //Rotacion
            kickVector = new Vector3(transform.position.x + (radius * effectSlider.value), transform.position.y, transform.position.z - (radius - (radius * effectSlider.value))) - transform.position;
            rotAxis = Vector3.Cross(kickVector, force);
            angularVel = rotAxis / inertiaMoment * timeForceAplied;

            if (drawArrows) {
                Quaternion rot = Quaternion.AngleAxis(Mathf.Acos(Vector3.Dot(Vector3.back, -shootDir)) * Mathf.Rad2Deg, Vector3.Cross(Vector3.back, -shootDir).normalized);
                gArrw = Instantiate(greyArrow, transform.position, rot) as GameObject;
                gArrw.transform.localScale = new Vector3(1, 1, Vector3.Distance(transform.position, target.position));

                rot = Quaternion.AngleAxis(Mathf.Acos(Vector3.Dot(Vector3.back, totalForce)) * Mathf.Rad2Deg, Vector3.Cross(Vector3.back, totalForce).normalized);
                rArrw = Instantiate(redArrow, transform.position, rot) as GameObject;

                rot = Quaternion.AngleAxis(Mathf.Acos(Vector3.Dot(Vector3.back, endVel)) * Mathf.Rad2Deg, Vector3.Cross(Vector3.back, endVel).normalized);
                grnArrw = Instantiate(greenArrow, transform.position, rot) as GameObject;

                bPnts = Instantiate(bluePoints, transform) as GameObject;
            }

        }

        if (Input.GetKey(KeyCode.Z)) {
            effectSlider.value -= 0.05f;
        } else if (Input.GetKey(KeyCode.X)) {
            effectSlider.value += 0.05f;
        }

        if (shooting) {

            //Rotacion
            magnusForce.x = km * initVel.magnitude * (angularVel.y * initVel.z - initVel.y * angularVel.z);
            magnusForce.y = km * initVel.magnitude * (angularVel.x * initVel.z - initVel.x * angularVel.z);
            magnusForce.z = km * initVel.magnitude * (angularVel.x * initVel.y - initVel.x * angularVel.y);

            totalForce = (magnusForce + gravity) / mass;

            //Movimiento
            endVel = initVel;
            initVel = endVel + totalForce * Time.deltaTime;
            transform.position += endVel;

            transform.Rotate(rotAxis, angularVel.magnitude);
            angularText.text = (angularVel.magnitude * Mathf.Rad2Deg).ToString();

            if (drawArrows) {

                Quaternion rot = Quaternion.AngleAxis(Mathf.Acos(Vector3.Dot(Vector3.back, totalForce)) * Mathf.Rad2Deg, Vector3.Cross(Vector3.back, totalForce).normalized);
                rArrw.transform.position = transform.position;

                rot = Quaternion.AngleAxis(Mathf.Acos(Vector3.Dot(Vector3.back, -endVel)) * Mathf.Rad2Deg, Vector3.Cross(Vector3.back, -endVel).normalized);
                grnArrw.transform.position = transform.position;
            }

            //El pulpo tiene que detener la bola
            if (needStop) {

                ballTarget.position = new Vector3(transform.position.x, transform.position.y, ballTarget.position.z);

                //El pulpo para la bola
                if (Vector3.Distance(transform.position, ballTarget.position) < 5 && needStop) {

                    transform.position = ballTarget.position;
                    angularVel = Vector3.zero;

                }

            }

            if (Vector3.Distance(initPos, transform.position) > maxDistanceBall) {
                shooting = false;
                transform.position = initPos;
                forceSlider.value = 0;
                cogiendoFuerza = false;
                target.position = initTargetPos;
                needStop = !needStop;
                if (gArrw != null) GameObject.Destroy(gArrw);
                if (bPnts != null) GameObject.Destroy(bPnts);
                if (rArrw != null) GameObject.Destroy(rArrw);
                if (grnArrw != null) GameObject.Destroy(grnArrw);
            }

        }
        
    }

}
