﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;




namespace OctopusController {

    public delegate float ErrorFunction(Vector3 target, float[] solution);

    public struct PositionRotation
    {
        Vector3 position;
        Quaternion rotation;

        public PositionRotation(Vector3 position, Quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }

        // PositionRotation to Vector3
        public static implicit operator Vector3(PositionRotation pr)
        {
            return pr.position;
        }
        // PositionRotation to Quaternion
        public static implicit operator Quaternion(PositionRotation pr)
        {
            return pr.rotation;
        }
    }


    internal class MyTentacleController

    //MAINTAIN THIS CLASS AS INTERNAL
    {

        Transform[] _bones;
        Transform[] _endEffectorSphere;

        public Transform[] Bones { get => _bones; }

        //Exercise 1.
        public Transform[] LoadTentacleJoints(Transform root)
        {
            //TODO: add here whatever is needed to find the bones forming the tentacle
            //you may want to use a list, and then convert it to an array
            List<Transform> bonesList = new List<Transform>();

            bonesList.Add(root.GetChild(0).GetChild(0));
            int i = 0;
            while (bonesList[i].childCount > 0)
            {
                i++;
                bonesList.Add(bonesList[i - 1].GetChild(0));
            }

            //TODO: in _endEffectorphere you  keep a reference to the sphere with a collider attached to the endEffector
            _endEffectorSphere = new Transform[1];
            _endEffectorSphere[0] = bonesList[i];
            bonesList.RemoveAt(i);

            _bones = bonesList.ToArray();

            distances = new float[_bones.Length - 1];
            copy = new Vector3[_bones.Length];

            for (i = 0; i < _bones.Length; i++) {
                copy[i] = _bones[i].position;
                if (i < _bones.Length - 1) distances[i] = Vector3.Distance(_bones[i].position, _bones[i + 1].position);
            }

            theta = new float[_bones.Length];
            sin = new float[_bones.Length];
            cos = new float[_bones.Length];


            solution = new float[_bones.Length];
            ErrorFunction = DistanceFromTarget;
            StartOffsets = new Vector3[_bones.Length];
            for (i = 0; i < _bones.Length; i++) {
                StartOffsets[i] = _bones[i].localPosition;
            }

            axisRotations = new Vector3[2];
            axisRotations[0] = Vector3.right;
            axisRotations[1] = Vector3.forward;

            jointAxisRotation = new Vector3[_bones.Length];
            for (i = 0; i < jointAxisRotation.Length; i++) {
                jointAxisRotation[i] = axisRotations[i % axisRotations.Length];
            }


            return Bones;
        }

        void MoveRandomly(IKMode mode)
        {


        }

        private Vector3[] copy;
        private float[] distances;
        public bool done;
        float threshold_distance = 0.1f;

        public void Movement_FABRIK(Transform target)
        {
            done = Vector3.Distance(copy[copy.Length - 1], target.position) < threshold_distance;
            if (!done)
            {
                float targetRootDist = Vector3.Distance(copy[0], target.position);

                // Update joint positions
                if (targetRootDist > distances.Sum())
                {
                    // The target is unreachable
                    for (int i = 1; i < _bones.Length; i++)
                    {
                        Vector3 direction = (target.position - copy[i - 1]).normalized;
                        copy[i] = copy[i - 1] + direction * distances[i - 1];
                    }
                }
                else
                {
                    // The target is reachable
                    while (!done)
                    {
                        // STAGE 1: FORWARD REACHING
                        copy[copy.Length - 1] = target.position;
                        for (int i = copy.Length - 1; i > 0; i--)
                        {
                            Vector3 temp = (copy[i - 1] - copy[i]).normalized;
                            temp = temp * distances[i - 1];
                            copy[i - 1] = temp + copy[i];
                        }

                        // STAGE 2: BACKWARD REACHING
                        copy[0] = _bones[0].position;
                        for (int i = 0; i < copy.Length - 2; i++)
                        {
                            Vector3 temp = (copy[i + 1] - copy[i]).normalized;
                            temp = temp * distances[i];
                            copy[i + 1] = temp + copy[i];
                        }

                        done = Vector3.Distance(copy[copy.Length - 1], target.position) < 0.001f;
                    }
                }

                // Update original joint rotations
                for (int i = 0; i <= _bones.Length - 2; i++)
                {

                    Vector3 vec1 = _bones[i + 1].position - _bones[i].position;
                    Vector3 vec2 = copy[i + 1] - copy[i];
                    Vector3 axis = Vector3.Cross(vec1, vec2).normalized;

                    float vCos = Vector3.Dot(vec1, vec2) / (vec1.magnitude * vec2.magnitude);
                    float vSin = Vector3.Cross(vec1.normalized, vec2.normalized).magnitude;

                    float angle = Mathf.Atan2(vSin, vCos) / 2;

                    _bones[i].rotation = new Quaternion(axis.x * Mathf.Sin(angle), axis.y * Mathf.Sin(angle), axis.z * Mathf.Sin(angle), Mathf.Cos(angle)) * _bones[i].rotation;
                    _bones[i].position = copy[i];

                    Quaternion quat = _bones[i].localRotation;
                    Quaternion qTwist = new Quaternion(0, quat.y, 0, quat.w).normalized;
                    _bones[i].localRotation = quat * Quaternion.Inverse(qTwist);

                }
            }
        }


        private float[] sin, cos, theta;
        private float epsilon = 0.1f;

        public void Movement_CCD(Transform target, float maxSwing, float minSwing, float maxTwist, float minTwist) {

            done = Vector3.Distance(_bones[_bones.Length - 1].position, target.position) < threshold_distance;

            if (!done) {

                for (int i = _bones.Length - 2; i > 0; i--) {

                    Vector3 r1 = _bones[_bones.Length - 1].position - _bones[i].position;
                    Vector3 r2 = target.position - _bones[i].position;

                    cos[i] = Vector3.Dot(r1, r2) / (r1.magnitude * r2.magnitude);
                    sin[i] = Vector3.Cross(r1, r2).magnitude / (r1.magnitude * r2.magnitude);

                    Vector3 axis = Vector3.Cross(r1, r2).normalized;

                    theta[i] = Mathf.Acos(cos[i]);

                    if (sin[i] < 0) theta[i] = -theta[i];
                    theta[i] *= Mathf.Rad2Deg;

                    _bones[i].rotation = Quaternion.AngleAxis(theta[i], axis) * _bones[i].rotation;

                    //CONSTRAINTS
                    if (i > 0) {
                        Vector3 ToParent = (_bones[i - 1].position - _bones[i].position).normalized;
                        Vector3 ToChild = (_bones[i + 1].position - _bones[i].position).normalized;
                        axis = Vector3.Cross(ToParent, ToChild).normalized;

                        float sin = Vector3.Cross(ToParent, ToChild).magnitude / (ToParent.magnitude * ToChild.magnitude);
                        float angle = Mathf.Acos(Vector3.Dot(ToParent, ToChild) / (ToParent.magnitude * ToChild.magnitude));
                        if (sin < 0) angle = -angle;
                        angle *= Mathf.Rad2Deg;

                        if (angle > maxSwing || angle < minSwing) {
                            angle = Mathf.Clamp(angle, minSwing, maxSwing);

                            Quaternion qrot = Quaternion.AngleAxis(angle, axis);
                            _bones[i].rotation = _bones[i - 1].rotation;
                            _bones[i].Rotate(axis, 180 + angle, Space.World);
                        }
                    } else if (i == 0) {
                        Vector3 ToParent = Vector3.down;
                        Vector3 ToChild = (_bones[i + 1].position - _bones[i].position).normalized;
                        axis = Vector3.Cross(ToParent, ToChild).normalized;

                        float angle = Mathf.Acos(Vector3.Dot(ToParent, ToChild) / (ToParent.magnitude * ToChild.magnitude)) * Mathf.Rad2Deg;

                        if (angle > maxSwing + maxSwing / 4 || angle < maxSwing - maxSwing / 4) {
                            angle = Mathf.Clamp(angle, maxSwing - maxSwing / 4, maxSwing + maxSwing / 4);

                            Quaternion qrot = Quaternion.AngleAxis(angle, axis);
                            _bones[i].rotation = Quaternion.identity;
                            _bones[i].Rotate(axis, 180 + angle, Space.World);
                        }
                    }

                    Quaternion quat = _bones[i].localRotation;
                    Quaternion qTwist = new Quaternion(0, quat.y, 0, quat.w).normalized;
                    Vector3 axisTwist = new Vector3();
                    float angleTwist = 0;
                    qTwist.ToAngleAxis(out angleTwist, out axisTwist);
                    Mathf.Clamp(angleTwist, minTwist, maxTwist);
                    qTwist = Quaternion.AngleAxis(angleTwist, axisTwist);
                    _bones[i].localRotation = quat * Quaternion.Inverse(qTwist);

                }
            }

        }


        public ErrorFunction ErrorFunction;
        float[] solution;
        Vector3 target;
        float DistanceFromDestination = 0;
        float StopThreshold = 0.1f;
        float SlowdownThreshold = 0.25f;
        float DeltaGradient = 0.1f;
        float LearningRate = 0.1f;
        public Vector3[] StartOffsets;

        private Vector3[] axisRotations;
        private Vector3[] jointAxisRotation;

        public void Movement_Gradient(Transform _target, float maxSwing, float minSwing) {

            Vector3 direction = (_target.position - _bones[0].position).normalized;
            target = _target.position - direction * DistanceFromDestination;

            if (ErrorFunction(target, solution) > StopThreshold) ApproachTarget(target, maxSwing, minSwing);

        }

        public float DistanceFromTarget(Vector3 target, float[] Solution) {
            Vector3 point = ForwardKinematics(Solution);
            return Vector3.Distance(point, target);
        }

        public void ApproachTarget(Vector3 target, float maxSwing, float minSwing) {
            for (int i = _bones.Length - 1; i >= 0; i--) {
                float error = ErrorFunction(target, solution);
                float slowDown = Mathf.Clamp01((error - StopThreshold) / (SlowdownThreshold - StopThreshold));
                float gradient = CalculateGradient(target, solution, i, DeltaGradient);
                solution[i] -= LearningRate * gradient * slowDown;
                //solution[i] = _bones[i].ClampAngle(solution[i]);
                solution[i] = Mathf.Clamp(solution[i], minSwing, maxSwing);
                if (ErrorFunction(target, solution) <= StopThreshold) break;
            }

            for (int i = 0; i < _bones.Length - 1; i++) {
                //_bones[i].MoveArm(solution[i]);
                float angle = Mathf.Clamp(solution[i], minSwing, maxSwing);
                if(jointAxisRotation[i] == Vector3.right) {
                    _bones[i].localEulerAngles = new Vector3(angle, 0, 0);
                } else if (jointAxisRotation[i] == Vector3.forward) {
                    _bones[i].localEulerAngles = new Vector3(0, 0, angle);
                }
            }

        }

        public float CalculateGradient(Vector3 target, float[] Solution, int i, float delta) {
            float solutionAngle = Solution[i];
            float f_x = ErrorFunction(target, Solution);
            Solution[i] += delta;
            float f_x_plus_h = ErrorFunction(target, Solution);
            float gradient = (f_x_plus_h - f_x) / delta;
            Solution[i] = solutionAngle;
            return gradient;
        }

        public PositionRotation ForwardKinematics(float[] Solution) {
            Vector3 prevPoint = _bones[0].transform.position;
            
            Quaternion rotation = _bones[0].rotation;

            for (int i = 1; i < _bones.Length; i++) {
                rotation *= Quaternion.AngleAxis(Solution[i - 1], jointAxisRotation[i - 1]);
                Vector3 nextPoint = prevPoint + rotation * StartOffsets[i];
                prevPoint = nextPoint;
            }

            // The end of the effector
            return new PositionRotation(prevPoint, rotation);
        }

    }
}