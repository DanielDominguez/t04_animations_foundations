Exercise 1

Overall, I think method FABRIK works best for this scenario because... 

Porque es el mejor que nos esta funcionando, ya que ha sido el masfácill de implementar porque unciamentedebíamoss anular el twist en el eje y.



Exercise 2

In Exercise 2 I used IK method FABRIK, implemented in the octopus of team BLUE.

Elegimos Fabrik porque al ser el método mas difícil de realizar constrains, y ya que en este ejercicio únicamente pedía restringir el twist para que las ventosas de los
tentáculos siempre estuvieran mirando a cámara, tras meditarlo mucho concluimos que era lo mas adecuado.



Exercise 3

In Exercise 3 I used IK method CCD, implemented in the octopus of team RED. The limits I
found interesting where to have a twist limited between 0º and 45º, and a swing limited
between 170º and 190º. 

Utilizamos CCD porque como los calculos de las nuevas posiciones los realizamos a través de ángulos, son mucho mas fáciles de realizar las constrains.



Exercise 4

In Exercise 4 I used IK method GRADIENT, implemented in the octopus of team YELLOW.

Hemos intentado utilizar gradient, ya que era el ultimo método que faltaba y el ejercicio especificaba que en cada equipo utilizábamos un método IK diferente, pero, 
pese que provamos que cada joint estuviera limitado a un único eje distinto al anterior, no hemos logrado un resultado satisfactorio.



Eduard Gaona Montes
Daniel Domínguez Hernández