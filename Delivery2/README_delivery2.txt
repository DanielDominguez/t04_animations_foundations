Exercise 1

Overall, I think method FABRIK works best for this scenario because... 

Porque es el mejor que nos esta funcionando, ya que ha sido el masf�cill de implementar porque unciamentedeb�amoss anular el twist en el eje y.



Exercise 2

In Exercise 2 I used IK method FABRIK, implemented in the octopus of team BLUE.

Elegimos Fabrik porque al ser el m�todo mas dif�cil de realizar constrains, y ya que en este ejercicio �nicamente ped�a restringir el twist para que las ventosas de los
tent�culos siempre estuvieran mirando a c�mara, tras meditarlo mucho concluimos que era lo mas adecuado.



Exercise 3

In Exercise 3 I used IK method CCD, implemented in the octopus of team RED. The limits I
found interesting where to have a twist limited between 0� and 45�, and a swing limited
between 170� and 190�. 

Utilizamos CCD porque como los calculos de las nuevas posiciones los realizamos a trav�s de �ngulos, son mucho mas f�ciles de realizar las constrains.



Exercise 4

In Exercise 4 I used IK method GRADIENT, implemented in the octopus of team YELLOW.

Hemos intentado utilizar gradient, ya que era el ultimo m�todo que faltaba y el ejercicio especificaba que en cada equipo utiliz�bamos un m�todo IK diferente, pero, 
pese que provamos que cada joint estuviera limitado a un �nico eje distinto al anterior, no hemos logrado un resultado satisfactorio.



Eduard Gaona Montes
Daniel Dom�nguez Hern�ndez